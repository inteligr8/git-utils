package com.inteligr8.git;

import java.io.IOException;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LocalRepositoryCacheUnitTest {

	private final static CredentialsProvider badCreds = new UsernamePasswordCredentialsProvider("not-a-valid-user", "not-a-valid-password");
	private final static CredentialsProvider gitCreds = new UsernamePasswordCredentialsProvider(
			System.getProperty("bitbucket.inteligr8.username"), System.getProperty("bitbucket.inteligr8.token"));
	
	private int cachedFiles;
	
	@Before
	public void compileStats() {
		this.cachedFiles = LocalRepositoryCache.getInstance().cacheDirectory.listFiles().length;
	}
	@After
	public void cleanupAndValidate() {
		LocalRepositoryCache.getInstance().clear();
		Assert.assertEquals(this.cachedFiles, LocalRepositoryCache.getInstance().cacheDirectory.listFiles().length);
	}
	
	/**
	 * Since "host-does-not-exist.com" does not exist, an UnknownHostException
	 * is thrown.  It is wrapped inside a JGit TransportException.
	 */
	@Test(expected = TransportException.class)
	public void tryBadHostRepo() throws GitAPIException, InterruptedException {
		LocalRepositoryCache.getInstance().acquire("https://host-does-not-exist.com/bmlong137/does-not-exist.git");
	}

	/**
	 * Since "does-not-exist" isn't a public repo, it requires authentication
	 * to even check to see if it is private.  This causes a
	 * NoRemoteRepositoryException which is wrapped inside a JGit
	 * InvalidRemoteException.  This is because SSH authentication makes this
	 * more like cacheNonExistentRepoAuth() than cacheNonExistentRepo().
	 */
	@Test(expected = InvalidRemoteException.class)
	public void tryNonExistentRepoViaSsh() throws GitAPIException, InterruptedException {
		LocalRepositoryCache.getInstance().acquire("git@github.com:bmlong137/does-not-exist.git");
	}
	
	/**
	 * Since "does-not-exist" isn't a public repo, it requires authentication
	 * to even check to see if it is private.  This causes a JSchException
	 * which is wrapped inside a JGit TransportException. 
	 */
	@Test(expected = TransportException.class)
	public void tryNonExistentRepo() throws GitAPIException, InterruptedException {
		LocalRepositoryCache.getInstance().acquire("https://github.com/bmlong137/does-not-exist.git");
	}

	/**
	 * Since "does-not-exist" isn't a repo, a NoRemoteRepositoryException is
	 * thrown.  It is wrapped inside a JGit InvalidRemoteException.  
	 */
	@Test(expected = InvalidRemoteException.class)
	public void tryNonExistentRepoAuth() throws GitAPIException, InterruptedException {
		LocalRepositoryCache.getInstance().acquire("https://bitbucket.org/inteligr8/does-not-exist.git", gitCreds);
	}
	
	/**
	 * Invalid credentials and repo doesn't actually exist 
	 */
	@Test(expected = InvalidRemoteException.class)
	public void tryInvalidCredsOnNoRepo() throws GitAPIException, InterruptedException {
		LocalRepositoryCache.getInstance().acquire("https://bitbucket.org/inteligr8/does-not-exist.git", badCreds);
	}
	
	/**
	 * Invalid credentials and public repo exists 
	 */
	@Test
	public void cacheInvalidCredsOnPublicRepo() throws GitAPIException, InterruptedException {
		Git git = LocalRepositoryCache.getInstance().acquire("https://bitbucket.org/inteligr8/git-utils.git", badCreds);
		try {
			this.validateGenericGitRepo(git);
		} finally {
			git.close();
		}
	}
	
	/**
	 * Invalid credentials and private repo exists 
	 */
	@Test(expected = TransportException.class)
	public void tryInvalidCredsOnPrivateRepo() throws GitAPIException, InterruptedException {
		LocalRepositoryCache.getInstance().acquire("https://bitbucket.org/inteligr8/propagate-branches.git", badCreds);
	}
	
	@Test
	public void cachePublicRepo() throws GitAPIException, InterruptedException {
		Git git = LocalRepositoryCache.getInstance().acquire("https://bitbucket.org/inteligr8/git-utils.git");
		try {
			this.validateGenericGitRepo(git);
		} finally {
			git.close();
		}
	}
	
	@Test
	public void cachePublicRepoViaSsh() throws GitAPIException, InterruptedException {
		Git git = LocalRepositoryCache.getInstance().acquire("git@bitbucket.org:inteligr8/git-utils.git");
		try {
			this.validateGenericGitRepo(git);
		} finally {
			git.close();
		}
	}

	/**
	 * Since "propagate-branches" isn't a public repo, it requires authentication
	 * to even check to see if it is private.  This causes a JSchException
	 * which is wrapped inside a JGit TransportException. 
	 */
	@Test(expected = TransportException.class)
	public void tryPrivateRepoUnauth() throws GitAPIException, InterruptedException {
		LocalRepositoryCache.getInstance().acquire("https://bitbucket.org/inteligr8/propagate-branches.git");
	}
	
	@Test
	public void cachePrivateRepo() throws GitAPIException, InterruptedException {
		Git git = LocalRepositoryCache.getInstance().acquire("https://bitbucket.org/inteligr8/propagate-branches.git", gitCreds);
		try {
			this.validateGenericGitRepo(git);
		} finally {
			git.close();
		}
	}
	
	@Test
	public void cachePublicRepoBranch() throws GitAPIException, InterruptedException, IOException {
		Git git = LocalRepositoryCache.getInstance().acquire("https://bitbucket.org/inteligr8/git-utils.git", "stable");
		try {
			this.validateGenericGitRepo(git);
			
			Assert.assertEquals("stable", git.getRepository().getBranch());
		} finally {
			git.close();
		}
	}
	
	@Test
	public void cacheReuse() throws GitAPIException, InterruptedException, IOException {
		Git git = LocalRepositoryCache.getInstance().acquire("https://bitbucket.org/inteligr8/git-utils.git");
		String repoId = git.getRepository().getIdentifier();
		git.close();
		
		Assert.assertEquals(this.cachedFiles+1, LocalRepositoryCache.getInstance().cacheDirectory.listFiles().length);
		Thread.sleep(1000L);
		Assert.assertEquals(this.cachedFiles+1, LocalRepositoryCache.getInstance().cacheDirectory.listFiles().length);
		
		git = LocalRepositoryCache.getInstance().acquire("https://bitbucket.org/inteligr8/git-utils.git");
		try {
			Assert.assertEquals(repoId, git.getRepository().getIdentifier());
			git.checkout().setName("develop").setStartPoint("origin/develop").setCreateBranch(true).call();
			Assert.assertEquals("develop", git.getRepository().getBranch());
		} finally {
			git.close();
		}
		
		Assert.assertEquals(this.cachedFiles+1, LocalRepositoryCache.getInstance().cacheDirectory.listFiles().length);
		Thread.sleep(1000L);
		Assert.assertEquals(this.cachedFiles+1, LocalRepositoryCache.getInstance().cacheDirectory.listFiles().length);
		
		git = LocalRepositoryCache.getInstance().acquire("https://bitbucket.org/inteligr8/git-utils.git", "stable");
		try {
			Assert.assertEquals("stable", git.getRepository().getBranch());
		} finally {
			git.close();
		}
	}
	
	private void validateGenericGitRepo(Git git) throws GitAPIException {
		Assert.assertNotNull(git);
		
		Repository repo = git.getRepository();
		Assert.assertTrue(repo.getDirectory().exists());
		Assert.assertTrue(repo.getWorkTree().exists());
		Assert.assertTrue(repo.getWorkTree().listFiles().length > 0);
	}

}
