package com.inteligr8.git;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ListBranchCommand.ListMode;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.util.FileUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class CommandUnitTest {
	
	private static File tmpdir;
	private static Git git;
	
	@BeforeClass
	public static void init() throws GitAPIException, IOException {
		tmpdir = new File(System.getProperty("java.io.tmpdir"), "git-" + UUID.randomUUID().toString() + ".tmp");
		
		git = new CloneCommand()
				.setURI("git@bitbucket.org:inteligr8/git-utils.git")
				.setDirectory(tmpdir)
				.call();
	}
	
	@AfterClass
	public static void cleanup() throws IOException {
		git.close();
		
		FileUtils.delete(tmpdir, FileUtils.RECURSIVE);
	}
	
	@Test
	public void listOfBranches() throws GitAPIException, IOException {
		List<Ref> remoteBranches = git.branchList().setListMode(ListMode.REMOTE).call();
		Assert.assertNotNull(remoteBranches);
		Assert.assertTrue(remoteBranches.contains(git.getRepository().findRef("origin/develop")));
		Assert.assertTrue(remoteBranches.contains(git.getRepository().findRef("origin/stable")));

		List<Ref> localBranches = git.branchList().call();
		Assert.assertNotNull(localBranches);
		Assert.assertEquals(1, localBranches.size());
		
		Assert.assertTrue(Collections.disjoint(remoteBranches, localBranches));
	}

}
