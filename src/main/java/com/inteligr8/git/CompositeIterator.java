/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.git;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * This class implements an iterator that spans multiple Java collections.
 * The {@link Iterator#remove()} method is implemented.
 * 
 * @author brian@inteligr8.com
 *
 * @param <E> A data type
 */
public class CompositeIterator<E> implements Iterator<E> {
	
	private final List<Iterator<E>> iterators = new LinkedList<Iterator<E>>();
	private final Iterator<Iterator<E>> iteratorIterators;
	private Iterator<E> iterator;
	
	/**
	 * This constructor initializes this object with an array of Java
	 * Collections.
	 *  
	 * @param cs An array of collections
	 */
	public CompositeIterator(@SuppressWarnings("unchecked") Collection<E>... cs) {
		for (Collection<E> c : cs)
			if (c != null && !c.isEmpty())
				this.iterators.add(c.iterator());
		this.iteratorIterators = this.iterators.iterator();
	}
	
	/**
	 * @see Iterator#hasNext() 
	 */
	@Override
	public boolean hasNext() {
		if (this.iterator == null) {
			if (!this.iteratorIterators.hasNext())
				return false;
			this.iterator = this.iteratorIterators.next();
		}
		
		while (!this.iterator.hasNext() && this.iteratorIterators.hasNext())
			this.iterator = this.iteratorIterators.next();
		return this.iterator.hasNext();
	}

	/**
	 * @see Iterator#next() 
	 */
	@Override
	public E next() {
		if (!this.hasNext())
			throw new NoSuchElementException();
		return this.iterator.next();
	}

	/**
	 * @see Iterator#remove() 
	 */
	@Override
	public void remove() {
		if (this.iterator != null)
			this.iterator.remove();
	}

}
