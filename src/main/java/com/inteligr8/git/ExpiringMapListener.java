/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.git;

import java.util.Map.Entry;

/**
 * This interface defines a set of listeners for expiring maps to use in a
 * publisher-subscription development model.
 * 
 * @author brian@inteligr8.com
 *
 * @param <K> A key data taype
 * @param <V> A value data type
 */
public interface ExpiringMapListener<K, V> extends MapListener<K, V> {

	/**
	 * This method allows the implementing class to listen for the expiring of
	 * something from a java.util.Map.  An expired entry does not trigger the
	 * remove() callback.
	 * 
	 * @param entry A map entry; never null; key is never null; value may be null
	 */
	void expired(Entry<K, V> entry);

}
