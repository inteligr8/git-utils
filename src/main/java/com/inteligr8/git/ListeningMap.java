/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.git;

import java.util.Map;

/**
 * This interface defines a means by which to assign map listeners to a map.
 * 
 * @author brian@inteligr8.com
 *
 * @param <K> A key data taype
 * @param <V> A value data type
 */
public interface ListeningMap<K, V> extends Map<K, V> {
	
	/**
	 * A method that allows for map listeners to be added to the implementing
	 * class.
	 * 
	 * @param listener A map listener.
	 */
	void addListener(MapListener<K, V> listener);

}
