/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.git;

import java.util.Map.Entry;

/**
 * This interface defines a set of listeners for maps to use in a
 * publisher-subscription development model.
 * 
 * @author brian@inteligr8.com
 *
 * @param <K> A key data taype
 * @param <V> A value data type
 */
public interface MapListener<K, V> {
	
	/**
	 * This method allows the implementing class to listen for the addition of
	 * something to a java.util.Map.
	 * 
	 * @param entry A map entry; never null; key is never null; value may be null
	 */
	void added(Entry<K, V> entry);

	/**
	 * This method allows the implementing class to listen for the accessing of
	 * something on a java.util.Map.  An access is not triggered by the
	 * containsKey() or containsValue() method.
	 * 
	 * @param entry A map entry; never null; key is never null; value may be null
	 */
	void accessed(Entry<K, V> entry);

	/**
	 * This method allows the implementing class to listen for the removal of
	 * something from a java.util.Map.  A clear() will not trigger this
	 * callback.
	 * 
	 * @param entry A map entry; never null; key is never null; value may be null
	 */
	void removed(Entry<K, V> entry);

	/**
	 * This method allows the implementing class to listen for the clearing of
	 * everything from a java.util.Map.  A remove() will not trigger this
	 * callback, even on the last removed entry.  This will be called for each
	 * and every entry that was on the map at the time of it being cleared.
	 * 
	 * @param entry A map entry; never null; key is never null; value may be null
	 */
	void cleared(Entry<K, V> entry);

}
