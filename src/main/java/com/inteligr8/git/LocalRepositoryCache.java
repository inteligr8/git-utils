/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.git;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.Semaphore;

import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ResetCommand.ResetType;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.RefNotFoundException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.util.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements a Git local repository cache with built-in expiration
 * capabilities.
 * 
 * @author brian@inteligr8.com
 */
@PublicApi
public class LocalRepositoryCache {
	
	private static final LocalRepositoryCache INSTANCE = new LocalRepositoryCache();
	
	/**
	 * This is a standard singleton accessor.
	 * 
	 * @return The only object of this class
	 */
	public static LocalRepositoryCache getInstance() {
		return INSTANCE;
	}
	
	
	
	private final Logger logger = LoggerFactory.getLogger(LocalRepositoryCache.class);
	
	private final LRUExpiringHashMap<String, CachedGit> cachedGits = new LRUExpiringHashMap<>(30);
	private final Map<String, InterruptableSemaphore> gitUrlSemaphores = new HashMap<>();
	final File cacheDirectory = new File(System.getProperty("java.io.tmpdir"), "git");
	private int simultaneousThreadsPerGitRepo = 1;
	
	private LocalRepositoryCache() {
		this.cacheDirectory.mkdir();
		this.cachedGits.addListener(new RepositoryCacheMapListener<CachedGit>());
	}
	
	/**
	 * This is helpful for the tweaking performance based on the environment of
	 * execution.  A value of 1 disables concurrency which results in the
	 * persistence of Git Repositories between executions and disables cache
	 * expiration.  This is great for development, testing, and command line
	 * usage.  It is not recommended with web service frontends.
	 */
	public void setSimultaneousThreadsPerGitRepository(int threads) {
		this.simultaneousThreadsPerGitRepo = threads;
	}
	
	@Override
	protected void finalize() throws Throwable {
		try {
			this.destroy();
		} finally { 
			super.finalize();
		}
	}
	
	private void destroy() {
    	if (this.logger.isDebugEnabled())
    		this.logger.debug("clearing local repo cache");
		this.clear();
	}
	
	/**
	 * @see LocalRepositoryCache#acquire(String, CredentialsProvider, String)
	 */
	public CachedGit acquire(String url) throws InterruptedException, InvalidRemoteException, TransportException, GitAPIException {
		try {
			return this.acquire(url, null, null);
		} catch (RefNotFoundException rnfe) {
			// this should never happen
			throw new DeveloperException(this.logger, rnfe);
		}
	}

	/**
	 * @see LocalRepositoryCache#acquire(String, CredentialsProvider, String)
	 */
	public CachedGit acquire(String url, CredentialsProvider creds) throws InterruptedException, InvalidRemoteException, TransportException, GitAPIException {
		try {
			return this.acquire(url, creds, null);
		} catch (RefNotFoundException rnfe) {
			// this should never happen
			throw new DeveloperException(this.logger, rnfe);
		}
	}

	/**
	 * @see LocalRepositoryCache#acquire(String, CredentialsProvider, String)
	 */
	public CachedGit acquire(String url, String branch) throws InterruptedException, InvalidRemoteException, RefNotFoundException, TransportException, GitAPIException {
		return this.acquire(url, null, branch);
	}
	
	/**
	 * This method checks out the specified Git repository.  The Git repository
	 * may have been retrieved from a cache.  On success, the Git repository
	 * HEAD is guaranteed to be clean and at the specified branch.  The branch
	 * will be current to the 'origin' remote no matter how long the Git
	 * repository has been cached.
	 * 
	 * You expected to close or release the CachedGit after complete.  A close
	 * will effectively release the object back to the cache.
	 *  
	 * @param url A URL to a Git Repository
	 * @param creds A username/password to the Git Repository; null will only work with public Git remotes
	 * @param branch A branch name of the branch in the Git Repository to start working on; null will let the Git remote decide
	 * @return A cached JGit Git object
	 * @throws InterruptedException The JVM was interrupted while the thread was waiting for another request on the same Git Repository
	 * @throws InvalidRemoteException The URL refers to nothing or an invalid Git Repository
	 * @throws RefNotFoundException The branch does not exist in the Git Repository represented by the URL
	 * @throws TransportException A disk or network error or hiccup occurred.
	 * @throws GitAPIException A Git API, library, or protocol related issue occurred
	 */
	public CachedGit acquire(String url, CredentialsProvider creds, String branch)
	throws InterruptedException, InvalidRemoteException, RefNotFoundException, TransportException, GitAPIException {
    	if (this.logger.isTraceEnabled())
    		this.logger.trace("acquire('" + url + "', " + creds + ", '" + branch + "')");
    	
    	// managing a semaphore per URL
    	// fringe case: multiple URLs could technically refer to the same repo; not a functional problem
    	InterruptableSemaphore semaphore = null;
    	synchronized (this) {
	    	semaphore = this.gitUrlSemaphores.get(url);
	    	if (semaphore == null)
	    		this.gitUrlSemaphores.put(url, semaphore = new InterruptableSemaphore(this.simultaneousThreadsPerGitRepo));
    	}
    	
    	File gitRepoDirectory = null;
    	
    	// enter thread-restricted block
    	semaphore.acquire();
    	try {
        	// only the specified number of threads will be allowed in here at the same time
    		
    		// have we already dealt with the specified Git repository in a prevoius request?  and was it returned to the cache? 
			CachedGit git = this.cachedGits.remove(url);
			if (git == null) {
				// we are here because:
				// - we have not seen this URL
				// - we have not seen this URL in a while (expired)
				// - we are in multi-threaded mode and another thread is working on the same remote Git repo
				
				// we need to use a UUID for the filename if supporting multiple threads
				String directoryBaseName;
				if (this.simultaneousThreadsPerGitRepo == 1)
					directoryBaseName = new GitUrl(url).getFullyQualifiedRepositoryName().replace('/', '_');
				else directoryBaseName = UUID.randomUUID().toString();
				
				gitRepoDirectory = new File(this.cacheDirectory, directoryBaseName + ".git");
		    	if (this.logger.isDebugEnabled())
		    		this.logger.debug("Git directory cache for clone: " + gitRepoDirectory);
				
		    	if (gitRepoDirectory.exists()) {
		    		// the Git repository already exists, but it shouldn't
		    		// it probably does from a previous JVM instance that was not properly cleaned up
			    	if (this.logger.isInfoEnabled())
			    		this.logger.info("Git Repository already exists; reusing: " + gitRepoDirectory);
		    		git = creds != null ? new CredentialedGit(Git.open(gitRepoDirectory), creds) : new ExtendedGit(Git.open(gitRepoDirectory));
		    	}
			}
			
			if (git == null) {
				// we are here for the same reasons above, except in the bad shutdown case
				
				// prepare the 'git clone' command
				CloneCommand clone = new CloneCommand()
		    			.setURI(url)
		    			.setDirectory(gitRepoDirectory);
				if (branch != null)
					clone.setBranch(branch);
				
				// git clone
		    	if (this.logger.isDebugEnabled())
		    		this.logger.debug("Cloning Git repository: " + url);
		    	git = creds != null ? new CredentialedGit(clone, creds) : new ExtendedGit(clone);
		    	if (this.logger.isInfoEnabled())
		    		this.logger.info("Cloned Git Repository: " + new GitUrl(git.getRemoteUrl("origin")).getFullyQualifiedRepositoryName());
			} else {
				// we are here because we already have a cloned Git repo
				
				// cleaning the current HEAD
		    	if (this.logger.isDebugEnabled())
		    		this.logger.debug("resetting Git");
				git.reset().setMode(ResetType.HARD).call();
				
				// getting latest remote refs from 'origin' remote
				git.fetch().call();
				
				if (branch != null && !branch.equals(git.getRepository().getBranch())) {
					// we care about the branch we are on, and we ain't on the right one
					
			    	if (this.logger.isDebugEnabled())
			    		this.logger.debug("switching Git branches: " + branch);
			    	if (git.getRepository().exactRef(Constants.R_HEADS + branch) == null) {
			    		// we have not checked this branch out before, so create it based on the 'origin' remote ref 
			    		git.checkout().setName(branch).setStartPoint("origin/" + branch).setCreateBranch(true).call();
			    	} else {
			    		git.checkout().setName(branch).call();
			    	}
				}
	
				// fast forward the local branch and HEAD to the latest remote ref (fetched earlier in this method)
		    	if (this.logger.isDebugEnabled())
		    		this.logger.debug("updating Git branch: " + git.getRepository().getBranch());
				git.pull().call();
			}

	    	return git;
    	} catch (URISyntaxException use) {
    		semaphore.release();
    		throw new IllegalArgumentException("The Git Repository URL is not properly formatted", use);
    	} catch (IOException ie) {
    		semaphore.release();
    		throw new TransportException("A I/O issue occurred", ie);
    	} catch (GitAPIException gae) {
    		semaphore.release();
    		throw gae;
    	}
    	
    	// the semaphore is not released on successful execution of this method on purpose
    	// the caller will operate on the Git repository with complete exclusion
    	// the caller is expected to "release" it when done
    	// the CachedGit will ensure it is released on "close" and at worst, during garbage collection  
	}
	
	/**
	 * This method releases the specified JGit Git object back to the cache.
	 * 
	 * @param git A cachable JGit Git object
	 */
	public void release(CachedGit git) {
		if (this.logger.isTraceEnabled())
    		this.logger.trace("release('" + git.getRepository().getIdentifier() + "')");
		
		try {
			String url = git.getRemoteUrl("origin").toString();
			
			synchronized (this) {
				Semaphore semaphore = this.gitUrlSemaphores.get(url);
				if (semaphore != null)
					semaphore.release();
			}
			
			this.cachedGits.put(url, git);
		} catch (GitAPIException gae) {
			if (this.logger.isDebugEnabled())
				this.logger.debug("A Git repository was never released", gae);
			this.logger.warn("A Git repository was never released, potentially blocking all future processing by this tool until a restart: " + git.toString());
		}
	}
	
	/**
	 * This method resets this cache as if it were a clean startup.  It will
	 * also remove the Git repositories from the local disk.  Any actively used
	 * Git repositories may continue.  Their subsequent release will result in
	 * them being added to the cache.  All threads waiting to acquire a Git
	 * repository will be interrupted resulting in an InterruptedException.
	 */
	public void clear() {
		if (this.logger.isTraceEnabled())
    		this.logger.trace("clear()");
		
		this.cachedGits.clear();
		
		synchronized (this) {
			for (InterruptableSemaphore semaphore : this.gitUrlSemaphores.values())
				semaphore.interruptQueuedThreads();
			this.gitUrlSemaphores.clear();
		}
	}
	
	private void expunge(Git git) {
		File gitDir = git.getRepository().getDirectory();
		File workingTreeDir = git.getRepository().getWorkTree();
		git.getRepository().close();
		
		try {
			if (this.logger.isDebugEnabled())
				this.logger.debug("deleting: " + gitDir);
			FileUtils.delete(gitDir, FileUtils.RECURSIVE);
		} catch (IOException ie) {
			this.logger.warn("Failed to delete a git directory: " + gitDir);
			if (this.logger.isDebugEnabled())
				this.logger.debug(ie.getMessage(), ie);
		}

		try {
			if (this.logger.isDebugEnabled())
				this.logger.debug("deleting: " + workingTreeDir);
			FileUtils.delete(workingTreeDir, FileUtils.RECURSIVE);
		} catch (IOException ie) {
			this.logger.warn("Failed to delete a git directory: " + workingTreeDir);
			if (this.logger.isDebugEnabled())
				this.logger.debug(ie.getMessage(), ie);
		}
		
		if (this.logger.isInfoEnabled())
			this.logger.info("Deleted Git Repository: " + gitDir);
	}
	
	
	
	private class RepositoryCacheMapListener<T extends CachedGit> implements ExpiringMapListener<String, T> {

		private final Logger logger = LoggerFactory.getLogger(LocalRepositoryCache.class);
		
		@Override
		public void accessed(Entry<String, T> entry) {
		}
		
		@Override
		public void added(Entry<String, T> entry) {
			// a clean one or one returned after being previously removed
		}
		
		@Override
		public void expired(Entry<String, T> entry	) {
			if (this.logger.isTraceEnabled())
	    		this.logger.trace("expired('" + entry.getKey() + "', '" + entry.getValue().getRepository().getIdentifier() + "')");
			expunge(entry.getValue());
		}
		
		@Override
		public void removed(Entry<String, T> entry) {
			// expected to be removed only temporarily...for use elsewhere; do not close
		}
		
		@Override
		public void cleared(Entry<String, T> entry) {
			if (this.logger.isTraceEnabled())
	    		this.logger.trace("cleared('" + entry.getKey() + "', '" + entry.getValue().getRepository().getIdentifier() + "')");
			expunge(entry.getValue());
		}
		
	}

}
