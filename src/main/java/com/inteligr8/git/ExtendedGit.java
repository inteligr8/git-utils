/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.git;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ListBranchCommand;
import org.eclipse.jgit.api.ListBranchCommand.ListMode;
import org.eclipse.jgit.api.ResetCommand.ResetType;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.RemoteConfig;
import org.eclipse.jgit.transport.URIish;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class extends the {@link CachedGit} class to include additional utility
 * functions.
 * 
 * @author brian@inteligr8.com
 */
@PublicApi
public class ExtendedGit extends CachedGit {

	private final Logger logger = LoggerFactory.getLogger(ExtendedGit.class);
	
	/**
	 * @see CachedGit#CachedGit(Git)
	 */
	public ExtendedGit(Git git) {
		super(git.getRepository());
	}

	/**
	 * @see CachedGit#CachedGit(CloneCommand)
	 */
	public ExtendedGit(CloneCommand clone) throws TransportException, InvalidRemoteException, GitAPIException {
		super(clone.call().getRepository());
	}

	/**
	 * @see CachedGit#CachedGit(Repository)
	 */
	public ExtendedGit(Repository repo) {
		super(repo);
	}
	
	/**
	 * This method finds the Git remotes that exist on the specified hostname.
	 * 
	 * @param hostname A Git URL hostname
	 * @return A collection of JGit remotes
	 * @throws GitAPIException A JGit exception
	 */
	public Collection<RemoteConfig> findRemotesByHostname(String hostname) throws GitAPIException {
		List<RemoteConfig> matchingRemotes = new LinkedList<>();
		
		List<RemoteConfig> remotes = this.remoteList().call();
		for (RemoteConfig remote : remotes) {
			for (URIish uri : remote.getURIs()) {
				GitUrl url = new GitUrl(uri);
				if (hostname.equalsIgnoreCase(url.getHostname())) {
					matchingRemotes.add(remote);
					break;
				}
			}
		}
		
		return matchingRemotes;
	}
	
	/**
	 * This method finds the Git remotes that contain the specified branch.
	 *  
	 * @param branchName A Git branch name
	 * @return A collection of Git remote names
	 * @throws IOException An I/O related issue occurred
	 * @throws GitAPIException A JGit exception occurred
	 */
	public Collection<String> findRemoteNames(String branchName) throws IOException, GitAPIException {
		List<String> remoteNames = new LinkedList<>();
		
		List<RemoteConfig> remotes = this.remoteList().call();
		for (RemoteConfig remote : remotes)
			if (this.getRepository().exactRef(Constants.R_REMOTES + remote.getName() + "/" + branchName) != null)
				remoteNames.add(remote.getName());
		
		return remoteNames;
	}

	/**
	 * This method finds the Git remote refs for the specified branch.  A
	 * remote ref is in the format "refs/remotes/{remote}/{branch}".
	 *  
	 * @param branchName A Git branch name
	 * @return A collection of Git remote refs
	 * @throws IOException An I/O related issue occurred
	 * @throws GitAPIException A JGit exception occurred
	 */
	public Collection<Ref> findRemoteRefsByName(String branchName) throws IOException, GitAPIException {
		List<Ref> refs = new LinkedList<>();
		
		List<RemoteConfig> remotes = this.remoteList().call();
		for (RemoteConfig remote : remotes) {
			Ref ref = this.getRepository().exactRef(Constants.R_REMOTES + remote.getName() + "/" + branchName);
			if (ref != null)
				refs.add(ref);
		}
		
		return refs;
	}

	/**
	 * This method effectively places the Git HEAD at the specified ref.  It
	 * will do this by reseting the current state and switching the HEAD to the
	 * specified ref.  This is useful when you are dealing with a formerly
	 * cached Git repository and you need it in a clean state.
	 *  
	 * @param ref A Git local ref
	 * @return The same Git local ref
	 * @throws IllegalArgumentException The ref must already exist
	 * @throws IOException An I/O related issue occurred
	 * @throws GitAPIException A JGit exception occurred
	 */
	public Ref resetAndCheckout(Ref ref) throws IOException, GitAPIException {
		if (!ref.getName().startsWith(Constants.R_HEADS))
			throw new IllegalArgumentException("The '" + ref + "' is not a local ref");
		return this._resetAndCheckout(ref);
		
	}

	/**
	 * This method effectively places the Git HEAD at the specified branch.  It
	 * will do this by reseting the current state and switching the HEAD to the
	 * specified ref.  This is useful when you are dealing with a formerly
	 * cached Git repository and you need it in a clean state.
	 *  
	 * @param branchName A Git branch name
	 * @return A Git local ref
	 * @throws IllegalArgumentException The branch must have already been checked out
	 * @throws IOException An I/O related issue occurred
	 * @throws GitAPIException A JGit exception occurred
	 */
	public Ref resetAndCheckout(String branchName) throws IOException, GitAPIException {
		if (this.logger.isTraceEnabled())
			this.logger.trace("resetOrCheckout('" + branchName + "')");
		
		Ref localRef = this.getRepository().exactRef(Constants.R_HEADS + branchName);
		if (localRef == null)
			throw new IllegalArgumentException("The '" + branchName + "' branch must already exist");
		return this._resetAndCheckout(localRef);
	}
	
	private Ref _resetAndCheckout(Ref ref) throws IOException, GitAPIException {
		Ref localRef = ref;
		
		if (!this.status().call().isClean()) {
			if (this.logger.isTraceEnabled())
				this.logger.trace("resetOrCheckout('" + ref + "'): repo is dirty");
			localRef = this.reset().setMode(ResetType.HARD).call();
		}
		
		if (!this.getRepository().getFullBranch().equals(localRef.getName())) {
			if (this.logger.isTraceEnabled())
				this.logger.trace("resetOrCheckout('" + ref + "'): repo is on a different branch");
			localRef = this.checkout().setName(localRef.getName()).call();
		}
		
		return localRef;
	}
	
	/**
	 * This method retrieves all branches, but excludes remote branches that are tracked with a local branch.
	 * 
	 * @return A list of Git refs
	 */
	@Deprecated
	public List<Ref> getAllUniqueBranches() throws GitAPIException {
		return this.getAllUniqueBranches(null);
	}
	
	/**
	 * This method retrieves all branches, but excludes remote branches that are tracked with a local branch.
	 * 
	 * @return A list of Git refs
	 */
	@Deprecated
	public List<Ref> getAllUniqueBranches(String containsCommitish) throws GitAPIException {
		if (this.logger.isTraceEnabled())
			this.logger.trace("getAllUniqueBranches('" + containsCommitish + "')");
		
		ListBranchCommand command = this.branchList().setListMode(ListMode.ALL);
		if (containsCommitish != null)
			command.setContains(containsCommitish);
		List<Ref> branches = command.call();

		if (this.logger.isTraceEnabled())
			this.logger.trace("getAllUniqueBranches('" + containsCommitish + "'): branch count: " + branches.size());
		
		Set<String> abbrevBranchNames = new HashSet<String>(branches.size());
		List<Ref> trimmedBranches = new ArrayList<Ref>(branches.size());
		
		// sort them by local, then remote; keep order the same otherwise
		Collections.sort(branches, new Comparator<Ref>() {
			@Override
			public int compare(Ref o1, Ref o2) {
				if (o1 == null) return -1;
				else if (o2 == null) return 1;
				else {
					boolean o1local = o1.getName().startsWith(Constants.R_HEADS);
					boolean o2local = o2.getName().startsWith(Constants.R_HEADS);
					if (o1local && o2local) return 0;
					else if (o1local) return -1;
					else if (o2local) return 1;
					else return 0;
				}
			}
		});

		if (this.logger.isTraceEnabled())
			this.logger.trace("getAllUniqueBranches('" + containsCommitish + "'): sorted");

		for (Ref branch : branches) {
			String fqBranchName = branch.getName();
			
			String abbrevBranchName = null;
			if (fqBranchName.startsWith(Constants.R_REMOTES)) {
				if (this.logger.isDebugEnabled())
					this.logger.debug("Ref is a remote branch: " + fqBranchName);
				abbrevBranchName = this.getRepository().shortenRemoteBranchName(fqBranchName);
			} else if (fqBranchName.startsWith(Constants.R_HEADS)) {
				if (this.logger.isDebugEnabled())
					this.logger.debug("Ref is a local branch: " + fqBranchName);
				abbrevBranchName = Repository.shortenRefName(fqBranchName);
			} else {
				if (this.logger.isDebugEnabled())
					this.logger.debug("Ref is not a local or remote branch: " + fqBranchName);
			}
			
			if (abbrevBranchName != null) {
				if (abbrevBranchNames.contains(abbrevBranchName)) {
					if (this.logger.isDebugEnabled())
						this.logger.debug("Branch already found; ignoring ...");
				} else {
					abbrevBranchNames.add(abbrevBranchName);
					trimmedBranches.add(branch);
				}
			}
		}

		if (this.logger.isTraceEnabled())
			this.logger.trace("getAllUniqueBranches('" + containsCommitish + "'): " + trimmedBranches);
		
		return trimmedBranches;
	}

}
