/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.git;

import java.net.URISyntaxException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jgit.transport.URIish;

/**
 * This class implements a wrapper around the JGit URI implementation.  Its
 * main purpose is to provide simple access to values parsed from the URL. 
 * 
 * @author brian@inteligr8.com
 */
@PublicApi
public class GitUrl {

	private static final Pattern gitUrlPattern = Pattern.compile("(((ssh|http(s)?)://([^@]+@)?([^:/]+)(:[0-9]+)?/)|git@([^:]+):)([\\w\\.@\\:/\\-~]+)(\\.git)");
	
	private final String url;
	private final String hostname;
	private final String fullyQualifiedRepositoryName;
	
	/**
	 * This constructor initializes this object using the JGit URI.
	 * 
	 * @param url A JGit URI
	 * @throws IllegalArgumentException The URL format does not match the expected pattern
	 */
	public GitUrl(URIish url) {
		this.url = url.toString();
		
		Matcher matcher = gitUrlPattern.matcher(url.toString());
		if (!matcher.find())
			throw new IllegalArgumentException("The '" + url + "' URL does not match the expected pattern: " + gitUrlPattern.toString());

		this.hostname = matcher.group(6) != null ? matcher.group(6) : matcher.group(8);
		this.fullyQualifiedRepositoryName = matcher.group(9);
	}

	/**
	 * This constructor initializes this object using a URL in the string
	 * format.  It will validate the URL format.
	 * 
	 * @param url A Git URL
	 * @throws IllegalArgumentException The URL format does not match the expected pattern
	 */
	public GitUrl(String url) throws URISyntaxException {
		this(new URIish(url));
	}
	
	/**
	 * @return A Git URL
	 */
	public String getUrl() {
		return this.url;
	}
	
	/**
	 * @return The hostname (no port) of the Git URL
	 */
	public String getHostname() {
		return this.hostname;
	}
	
	/**
	 * @return The fully qualified repository name.  This includes the full path specified after the hostname. 
	 */
	public String getFullyQualifiedRepositoryName() {
		return this.fullyQualifiedRepositoryName;
	}

}
