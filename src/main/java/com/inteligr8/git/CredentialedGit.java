/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.git;

import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.FetchCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.LsRemoteCommand;
import org.eclipse.jgit.api.PullCommand;
import org.eclipse.jgit.api.PushCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.CredentialsProvider;

/**
 * This class extends the {@link ExtendedGit} class to include authentication
 * related functionality.
 * 
 * @author brian@inteligr8.com
 */
@PublicApi
public class CredentialedGit extends ExtendedGit {
	
	private CredentialsProvider credProvider;

	/**
	 * @param git A JGit Git object
	 * @param creds A JGit credential provider
	 * @see CachedGit#CachedGit(Git)
	 */
	public CredentialedGit(Git git, CredentialsProvider creds) {
		super(git.getRepository());
		this.credProvider = creds;
	}
	
	/**
	 * @param clone A JGit CloneCommand object
	 * @param creds A JGit credential provider
	 * @throws TransportException A network exception occurred
	 * @throws InvalidRemoteException The specified remote URL does not reference a Git repository
	 * @throws GitAPIException A JGit exception occurred
	 * @see CachedGit#CachedGit(CloneCommand)
	 */
	public CredentialedGit(CloneCommand clone, CredentialsProvider creds) throws TransportException, InvalidRemoteException, GitAPIException {
		this(clone.setCredentialsProvider(creds).call(), creds);
	}

	/**
	 * @param repo A JGit Repository object
	 * @param creds A JGit credential provider
	 * @throws TransportException A network exception occurred
	 * @throws InvalidRemoteException The specified remote URL does not reference a Git repository
	 * @throws GitAPIException A JGit exception occurred
	 * @see CachedGit#CachedGit(Repository)
	 */
	public CredentialedGit(Repository repo, CredentialsProvider creds) {
		super(repo);
		this.credProvider = creds;
	}
	
	/**
	 * @return A JGit FetchCommand with embedded credentials
	 */
	@Override
	public FetchCommand fetch() {
		return super.fetch().setCredentialsProvider(this.credProvider);
	}

	/**
	 * @return A JGit LsRemoteCommand with embedded credentials
	 */
	@Override
	public LsRemoteCommand lsRemote() {
		return super.lsRemote().setCredentialsProvider(this.credProvider);
	}

	/**
	 * @return A JGit PullCommand with embedded credentials
	 */
	@Override
	public PullCommand pull() {
		return super.pull().setCredentialsProvider(this.credProvider);
	}

	/**
	 * @return A JGit PushCommand with embedded credentials
	 */
	@Override
	public PushCommand push() {
		return super.push().setCredentialsProvider(this.credProvider);
	}

}
