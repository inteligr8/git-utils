/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.git;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

/**
 * This class implements a priority, then FIFO (first-in/first-out) queue that
 * ignores entries when there is a duplicate already on the queue.
 * 
 * A duplicate is determined by the typical combination of hashCode() and
 * equals() of the objects in the queue.  The priority is determined by a
 * `java.util.Comparator` specified at queue construction. 
 * 
 * @author brian@inteligr8.com
 *
 * @param <T> The data type of the elements in the queue.  Uniqueness is determined by multiple possible factors.
 */
@PublicApi
public class UniquePriorityFifoQueue<T> implements Queue<T> {
	
	private final Counter counter = new SimpleCounter();
	private final Counter fakeCounter = new FakeCounter();
	private final TreeSet<PriorityFifoElement<T>> elements;
	// TreeSet does not check for uniqueness using hashCode, but using the comparator instead
	private final Set<PriorityFifoElement<T>> uelements;
	private final Object sync = new Object();

	/**
	 * This constructor defines a unique FIFO queue without any priority
	 * characteristics.
	 */
	public UniquePriorityFifoQueue() {
		this.elements = new TreeSet<PriorityFifoElement<T>>();
		this.uelements = new HashSet<PriorityFifoElement<T>>();
	}
	
	/**
	 * This constructor defines a unique FIFO queue without any priority
	 * characteristics.  It also stages the queue with objects from the
	 * specified collection.
	 *  
	 * @param c A collection of objects, whose data types are compatible with the queue's.
	 */
	public UniquePriorityFifoQueue(Collection<? extends T> c) {
		this();
		this.addAll(c);
	}
	
	/**
	 * This constructor defines a unique priority FIFO queue.  The priority is
	 * determined by the specified Comparator in ascending order.
	 * 
	 * @param priorityComparator A comparator.  "Less than" is of highest priority. 
	 */
	public UniquePriorityFifoQueue(final Comparator<? super T> priorityComparator) {
		this.elements = new TreeSet<PriorityFifoElement<T>>(new Comparator<PriorityFifoElement<T>>() {
			@Override
			public int compare(PriorityFifoElement<T> o1, PriorityFifoElement<T> o2) {
				int compare = priorityComparator.compare(o1.element, o2.element);
				if (compare != 0) return compare;
				return o1.compareTo(o2);
			}
		});
		this.uelements = new HashSet<PriorityFifoElement<T>>();
	}
	
	/**
	 * This method acts as specified in the Queue interface.  It is worth
	 * noting that it will return false if the specified object is a duplicate
	 * of an object already in the queue. 
	 * 
	 * @see Queue#add(Object)
	 */
	@Override
	public boolean add(T e) {
		PriorityFifoElement<T> element = new PriorityFifoElement<T>(e, this.counter);
		synchronized (this.sync) {
			return this.uelements.add(element) && this.elements.add(element);
		}
		
	}

	/**
	 * This method acts as specified in the Queue interface.  It is worth
	 * noting that it will return false if all the objects in the specified
	 * Collection are duplicates of objects already in the queue. 
	 * 
	 * @see Queue#addAll(Collection)
	 */
	@Override
	public boolean addAll(Collection<? extends T> c) {
		boolean anyAdded = false;
		if (c != null) for (T e : c) {
			PriorityFifoElement<T> element = new PriorityFifoElement<T>(e, this.counter);
			synchronized (this.sync) {
				if (this.uelements.add(element) && this.elements.add(element))
					anyAdded = true;
			}
		}
		return anyAdded;
	}
	
	/**
	 * @see Queue#clear()
	 */
	@Override
	public void clear() {
		synchronized (this.sync) {
			this.elements.clear();
			this.uelements.clear();
		}
	}

	/**
	 * @see Queue#contains(Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean contains(Object o) {
		return this.uelements.contains(new PriorityFifoElement<T>((T)o, this.fakeCounter));
	}

	/**
	 * @see Queue#containsAll(Collection)
	 */
	@Override
	public boolean containsAll(Collection<?> c) {
		if (c != null) for (Object o : c)
			if (!this.contains(o))
				return false;
		return true;
	}

	/**
	 * @see Queue#element()
	 */
	@Override
	public T element() {
		return this.elements.iterator().next().element;
	}

	/**
	 * @see Queue#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return this.elements.isEmpty();
	}
	
	/**
	 * This method does return an Interator that supports the remove() method.
	 * 
	 * @see Queue#iterator()
	 */
	@Override
	public Iterator<T> iterator() {
		final Iterator<PriorityFifoElement<T>> i = this.elements.iterator();
		return new Iterator<T>() {
			private T lastElement;
			
			@Override
			public boolean hasNext() {
				return i.hasNext();
			}
			
			@Override
			public T next() {
				this.lastElement = i.next().element;
				return this.lastElement;
			}
			
			@Override
			public void remove() {
				synchronized (sync) {
					i.remove();
					uelements.remove(this.lastElement);
				}
			}
		};
	}

	/**
	 * This method acts as specified in the Queue interface.  It is worth
	 * noting that it will return false if the specified object is a duplicate
	 * of an object already in the queue.
	 * 
	 * @see Queue#offer(Object)
	 * @see UniquePriorityFifoQueue#add(Object)
	 */
	@Override
	public boolean offer(T e) {
		// not a capacity restricting queue, so just use add()
		return this.add(e);
	}

	/**
	 * @see Queue#peek()
	 */
	@Override
	public T peek() {
		Iterator<PriorityFifoElement<T>> i = this.elements.iterator();
		return i.hasNext() ? i.next().element : null;
	}

	/**
	 * @see Queue#poll()
	 */
	@Override
	public T poll() {
		synchronized (this.sync) {
			PriorityFifoElement<T> element = this.elements.pollFirst();
			if (element == null)
				return null;
			this.uelements.remove(element);
			return element.element;
		}
	}

	/**
	 * @see Queue#remove()
	 */
	@Override
	public T remove() {
		synchronized (this.sync) {
			PriorityFifoElement<T> element = this.elements.pollFirst();
			if (element == null)
				throw new NoSuchElementException();
			this.uelements.remove(element);
			return element.element;
		}
	}

	/**
	 * @see Queue#remove(Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean remove(Object o) {
		PriorityFifoElement<T> element = new PriorityFifoElement<T>((T)o, this.fakeCounter);
		
		// doing the find before the sync block so the find doesn't block threads
		PriorityFifoElement<T> realElement = this.find(element.element);
		
		synchronized (this.sync) {
			this.uelements.remove(element);
			return this.elements.remove(realElement);
		}
	}

	/**
	 * @see Queue#removeAll(Collection)
	 */
	@Override
	public boolean removeAll(Collection<?> c) {
		if (c != null) for (Object o : c)
			this.remove(o);
		return true;
	}

	/**
	 * @see Queue#retainAll(Collection)
	 */
	@Override
	public boolean retainAll(Collection<?> c) {
		Iterator<PriorityFifoElement<T>> i = this.elements.iterator();
		while (i.hasNext()) {
			PriorityFifoElement<T> e = i.next();
			T element = e.element;
			if (!c.contains(element)) {
				synchronized (this.sync) {
					i.remove();
					this.uelements.remove(e);
				}
			}
		}
		
		return true;
	}

	/**
	 * @see Queue#size()
	 */
	@Override
	public int size() {
		return this.elements.size();
	}

	/**
	 * @see Queue#toArray()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object[] toArray() {
		Object[] objs = this.elements.toArray();
		Object[] ts = new Object[objs.length];
		for (int i = 0; i < objs.length; i++)
			ts[i] = ((PriorityFifoElement<T>)objs[i]).element;
		return ts;
	}

	/**
	 * @see Queue#toArray(Object[])
	 */
	@SuppressWarnings("unchecked")
	public <U extends Object> U[] toArray(U[] a) {
		if (a.length < this.elements.size())
			a = Arrays.copyOf(a, this.elements.size());

		int i = 0;
		for (PriorityFifoElement<T> element : this.elements)
			a[i] = (U)element.element;
		return a;
	}
	
	private PriorityFifoElement<T> find(T e) {
		for (PriorityFifoElement<T> element : this.elements) {
			if (element.element.equals(e))
				return element;
		}
		
		return null;
	}
	
	
	
	private class PriorityFifoElement<E> implements Comparable<PriorityFifoElement<E>> {
		
		private final E element;
		private final int fifoId;
		
		public PriorityFifoElement(E element, Counter counter) {
			this.element = element;
			synchronized (counter) {
				this.fifoId = counter.next();
			}
		}
		
		@Override
		public int compareTo(PriorityFifoElement<E> o) {
			if (this.fifoId < 0) return 0;
			else if (o == null) return 1;
			else if (o.fifoId < 0) return 0;
			else return this.fifoId - o.fifoId;
		}
		
		@Override
		public int hashCode() {
			return this.element.hashCode();
		}
		
		@SuppressWarnings("rawtypes")
		@Override
		public boolean equals(Object obj) {
			if (obj instanceof PriorityFifoElement) {
				return this.element.equals(((PriorityFifoElement)obj).element);
			} else {
				return false;
			}
		}
		
	}
	
	private interface Counter {
		
		int next();
		
	}
	
	public class FakeCounter implements Counter {
		
		public int next() {
			return -1;
		}
		
	}
	
	public class SimpleCounter implements Counter {
		
		private int count = 0;
		
		public synchronized int next() {
			this.count++;
			return this.count;
		}
		
	}

}
