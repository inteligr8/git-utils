/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.git;

import org.slf4j.Logger;

/**
 * This class implements an exception that is the fault of the developer; not
 * the user, system, network, infrastructure, etc...  The purpose of this is
 * similar to an assertion.
 * 
 * @author brian@inteligr8.com
 */
class DeveloperException extends RuntimeException {
	
	private static final long serialVersionUID = 1600616227925437703L;
	private static final String MESSAGE = "This is a bug that needs to be addressed by the developer";
	
	/**
	 * This constructor initializes this object using a logger and exception.
	 * @param logger A SLF4J logger
	 * @param cause An exception causing this exception
	 */
	public DeveloperException(Logger logger, Throwable cause) {
		super(MESSAGE, cause);
		logger.error(MESSAGE);
	}

}
