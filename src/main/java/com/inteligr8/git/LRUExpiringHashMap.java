/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.git;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This clsas implements a map that orders entries from least recently used
 * (LRU) to most recently used.  The LRU entries are subject to expiration.
 * This map also supports the publication-subscription development model with
 * listener registration.
 * 
 * TODO this is not a fully-complaint implementation of a Java Map
 * TODO need to implement the actual expiration schedule
 * 
 * @author brian@inteligr8.com
 * 
 * @param <K> A key data type
 * @param <V> A value data type
 */
public class LRUExpiringHashMap<K extends Serializable, V> implements ListeningMap<K, V> {

	private final Logger logger = LoggerFactory.getLogger(LRUExpiringHashMap.class);
	
	private final Object sync = new Object();
	private final LinkedHashMap<K, ExpiringValue> map;
	private final List<MapListener<K, V>> listeners = new LinkedList<MapListener<K, V>>();
	private final long expirationTimeMillis;
	
	/**
	 * This constructor creates a map with the specified expiration time
	 * limit for entries.
	 * 
	 * @param expirationTimeInMinutes A time in minutes
	 * @see LinkedHashMap#LinkedHashMap()
	 */
	public LRUExpiringHashMap(int expirationTimeInMinutes) {
		this.map = new LinkedHashMap<>();
		this.expirationTimeMillis = expirationTimeInMinutes * 60L + 1000L;
	}

	/**
	 * This constructor creates a map with the specified expiration time
	 * limit for entries and the specified initial capacity.
	 * 
	 * @param expirationTimeInMinutes A time in minutes
	 * @param initialCapacity The initial capacity
	 * @see LinkedHashMap#LinkedHashMap(int)
	 */
	public LRUExpiringHashMap(int expirationTimeInMinutes, int initialCapacity) {
		this.map = new LinkedHashMap<>(initialCapacity);
		this.expirationTimeMillis = expirationTimeInMinutes * 60L + 1000L;
	}
	
	/**
	 * @see ListeningMap#addListener(MapListener)
	 */
	@Override
	public void addListener(MapListener<K, V> listener) {
    	if (this.logger.isDebugEnabled())
    		this.logger.debug("adding listener: " + listener.getClass());
		this.listeners.add(listener);
	}
	
	/**
	 * @see Map#containsKey(Object)
	 */
	@Override
	public boolean containsKey(Object key) {
		return this.map.containsKey(key);
	}

	/**
	 * @see Map#containsValue(Object)
	 */
	@Override
	public boolean containsValue(Object value) {
		return this.map.containsValue(value);
	}

	/**
	 * @see Map#entrySet()
	 */
	@Override
	public Set<Entry<K, V>> entrySet() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * @see Map#keySet()
	 */
	@Override
	public Set<K> keySet() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see Map#values()
	 */
	@Override
	public Collection<V> values() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * @see Map#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return this.map.isEmpty();
	}
	
	/**
	 * @see Map#size()
	 */
	@Override
	public int size() {
		return this.map.size();
	}
	
	/**
	 * This method acts as defined by the {@link java.util.Map} interface.  Of
	 * note, it resets the expiration timer on the entry accessed.  It will
	 * also trigger any listener to {@link MapListener#accessed(java.util.Map.Entry)}.
	 * 
	 * @see Map#get(Object)
	 * @see MapListener#accessed(java.util.Map.Entry)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public V get(Object key) {
		ExpiringValue evalue;
		
		synchronized (this.sync) {
			if (!this.map.containsKey(key))
				return null;
			
			// remove and put to move the entry to the end of the map; helping with finding expired entries
			evalue = this.map.remove(key);
			this.map.put((K)key, new ExpiringValue(evalue.getValue(), System.currentTimeMillis() + this.expirationTimeMillis));
		}
		
		for (MapListener<K, V> listener : this.listeners)
			listener.accessed(new ExpiringHashMapEntry((K)key, evalue.getValue()));
		return evalue.getValue();
	}

	/**
	 * This method acts as defined by the {@link java.util.Map} interface.  Of
	 * note, it starts the expiration timer on the new or updated entry.  It
	 * will also trigger any listener to {@link MapListener#added(java.util.Map.Entry)}.
	 * 
	 * @see Map#put(Object, Object)
	 * @see MapListener#added(java.util.Map.Entry)
	 */
	@Override
	public V put(K key, V value) {
		ExpiringValue evalue = new ExpiringValue(value, System.currentTimeMillis() + this.expirationTimeMillis);
		ExpiringValue oldValue = this.map.put(key, evalue);
		for (MapListener<K, V> listener : this.listeners)
			listener.added(new ExpiringHashMapEntry(key, value));
		return oldValue == null ? null : oldValue.getValue();
	}

	/**
	 * This method acts as defined by the {@link java.util.Map} interface.  Of
	 * note, it will trigger any listener to {@link MapListener#cleared(java.util.Map.Entry)}.
	 * 
	 * @see Map#clear()
	 * @see MapListener#cleared(java.util.Map.Entry)
	 */
	@Override
	public void clear() {
		List<ExpiringHashMapEntry> entries = new LinkedList<>();
		
		synchronized (this.sync) {
			for (Entry<K, ExpiringValue> entry : this.map.entrySet())
				entries.add(new ExpiringHashMapEntry(entry.getKey(), entry.getValue().getValue()));
			this.map.clear();
		}

		for (ExpiringHashMapEntry entry : entries)
			for (MapListener<K, V> listener : this.listeners)
				listener.cleared(entry);
	}

	/**
	 * This method acts as defined by the {@link java.util.Map} interface.  Of
	 * note, it will trigger any listener to {@link MapListener#removed(java.util.Map.Entry)}.
	 * 
	 * @see Map#remove(Object)
	 * @see MapListener#removed(java.util.Map.Entry)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public V remove(Object key) {
		ExpiringValue evalue;
		
		synchronized (this.sync) {
			if (!this.map.containsKey(key))
				return null;
			evalue = this.map.remove(key);
		}
		
		for (MapListener<K, V> listener : this.listeners)
			listener.removed(new ExpiringHashMapEntry((K)key, evalue.getValue()));
		return evalue.getValue();
	}

	/**
	 * This method is currently not implemented.
	 * FIXME
	 */
	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * @see LinkedHashMap#hashCode()
	 */
	@Override
	public int hashCode() {
		return this.map.hashCode();
	}
	
	/**
	 * @see LinkedHashMap#equals(Object)
	 */
	@Override
	public boolean equals(Object obj) {
		return this.map.equals(obj);
	}
	
	/**
	 * This method scans the underlying map for expired entries.  Since the map
	 * is ordered, it will immediately find and expire all applicable entries
	 * on the O(e).  This method will also trigger any listener to
	 * {@link ExpiringMapListener#expired(java.util.Map.Entry)}.
	 * 
	 * At this time, expiration does not remove entries from the java.util.Map
	 * periodically.  This method must be used to enforce the removal. 
	 */
	public void exipriationCheck() {
		// since these should be order, we could break out of this loop once we reach an unexpired entry
		List<ExpiringHashMapEntry> entries = new LinkedList<>();
		
		synchronized (this.sync) {
			Iterator<Entry<K, ExpiringValue>> i = this.map.entrySet().iterator();
			for (Entry<K, ExpiringValue> entry = i.next(); i.hasNext(); entry = i.next()) {
				if (entry.getValue().isExpired()) {
					i.remove();
					entries.add(new ExpiringHashMapEntry(entry.getKey(), entry.getValue().getValue()));
				} else {
					// ordered map; ordered by time of entry
					// since expiration timers are constant, we can skip looping
					break;
				}
			}
		}

		for (ExpiringHashMapEntry entry : entries)
			for (MapListener<K, V> listener : this.listeners)
				if (listener instanceof ExpiringMapListener)
					((ExpiringMapListener<K, V>)listener).expired(entry);
	}
	
	/**
	 * This method explicitly expires the specified entry.  This will expire
	 * and remove the entry from the underlying map.  This is functionality
	 * equivalent to {@link Map#remove(Object)} except it will trigger any
	 * listener to {@link ExpiringMapListener#expired(java.util.Map.Entry)}.
	 * 
	 * @param key A map entry key
	 */
	public void expire(K key) {
    	if (this.logger.isDebugEnabled())
    		this.logger.debug("expiring key from map: " + key);

		ExpiringValue evalue;
		
    	synchronized (this.sync) {
			if (!this.map.containsKey(key))
				return;
			evalue = this.map.remove(key);
    	}
    	
		for (MapListener<K, V> listener : this.listeners)
			if (listener instanceof ExpiringMapListener)
				((ExpiringMapListener<K, V>)listener).expired(new ExpiringHashMapEntry(key, evalue.getValue()));
	}
	
	/**
	 * @see LinkedHashMap#toString()
	 */
	@Override
	public String toString() {
		return this.map.toString();
	}
	
	
	
	private class ExpiringHashMapEntry implements Entry<K, V> {
		
		private final K key;
		private V value;
		
		public ExpiringHashMapEntry(K key, V value) {
			this.key = key;
			this.value = value;
		}
		
		@Override
		public K getKey() {
			return this.key;
		}
		
		@Override
		public V getValue() {
			return this.value;
		}
		
		@Override
		public V setValue(V value) {
			return this.value = value;
		}
		
		@Override
		public boolean equals(Object obj) {
			if (obj instanceof Entry<?, ?>) {
				return this.key.equals(((Entry<?, ?>)obj).getKey()) && this.value.equals(((Entry<?, ?>)obj).getValue());
			} else {
				return false;
			}
		}
		
		@Override
		public int hashCode() {
			return this.key.hashCode() + this.value.hashCode();
		}
		
		@Override
		public String toString() {
			return "{" + this.key + ", " + this.value + "}";
		}
		
	}
	
	private class ExpiringValue {
	
		private long expirationTimeInMillis;
		private V value;
		
		public ExpiringValue(V value, long expirationTimeInMillis) {
			this.value = value;
			this.expirationTimeInMillis = expirationTimeInMillis;
		}
		
		public V getValue() {
			return this.value;
		}
		
		public boolean isExpired() {
			return this.expirationTimeInMillis <= System.currentTimeMillis();
		}
		
		@Override
		public String toString() {
			return this.value.toString();
		}
		
	}

}
