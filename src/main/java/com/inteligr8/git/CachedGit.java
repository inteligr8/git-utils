/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.git;

import java.util.List;

import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.RemoteConfig;
import org.eclipse.jgit.transport.URIish;

/**
 * This class extends the JGit {@link Git} class to include Git repository
 * caching on the local disk.  Cache entries are expunged by expiration of
 * the least recently accessed Git repositories.
 * 
 * WARNING A cached Git repository may be in a state different than what would
 * be expected of a fresh cloned Git repository.
 * 
 * @author brian@inteligr8.com
 */
@PublicApi
public class CachedGit extends Git {
	
	/**
	 * This constructor initializes this object using a JGit {@link Git} object.
	 * 
	 * @param git A JGit Git object
	 */
	public CachedGit(Git git) {
		super(git.getRepository());
	}

	/**
	 * This constructor initializes this object using a JGit
	 * {@link CloneCommand} object.  The clone will be performed before this
	 * method returns.
	 * 
	 * @param clone A JGit Git object
	 * @throws TransportException A network exception occurred
	 * @throws InvalidRemoteException The specified remote URL does not reference a Git repository
	 * @throws GitAPIException A JGit exception occurred
	 */
	public CachedGit(CloneCommand clone) throws TransportException, InvalidRemoteException, GitAPIException {
		super(clone.call().getRepository());
	}
	
	/**
	 * This constructor initializes this object using a JGit {@link Repository}
	 * object.
	 * 
	 * @param repo A JGit Repository object
	 */
	public CachedGit(Repository repo) {
		super(repo);
	}
	
	/**
	 * This method retrieves the URL of the specified Git remote.  All cloned
	 * repositories will have a default remote named 'origin'.  This is
	 * required for the unique identification of Git repositories by the
	 * caching mechanism.
	 * 
	 * @param remoteName The name of a Git remote defined on the repository.
	 * @return A JGit URI
	 * @throws GitAPIException A JGit exception
	 */
	public URIish getRemoteUrl(String remoteName) throws GitAPIException {
		List<RemoteConfig> configs = this.remoteList().call();
		for (RemoteConfig config : configs)
			if (config.getName().equals(remoteName))
				return config.getURIs().iterator().next();
		return null;
	}
	
	/**
	 * This method overrides the default behavior of {@link Git#close()},
	 * opting to return this Git repository to the cache instead.
	 */
	@Override
	public void close() {
		LocalRepositoryCache.getInstance().release(this);
	}

}
