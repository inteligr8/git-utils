/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.git;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * This class implements the ability to interrupt threads waiting for available
 * permits.
 * 
 * @author brian@inteligr8.com
 */
public class InterruptableSemaphore extends Semaphore {
	
	private static final long serialVersionUID = 8346747352241823763L;
	private final Object sync = new Object();
	
	/**
	 * @see Semaphore#Semaphore(int)
	 */
	public InterruptableSemaphore(int permits) {
		super(permits);
	}

	/**
	 * @see Semaphore#Semaphore(int, boolean)
	 */
	public InterruptableSemaphore(int permits, boolean fair) {
		super(permits, fair);
	}
	
	/**
	 * @see Semaphore#acquire()
	 */
	@Override
	public void acquire() throws InterruptedException {
		synchronized (this.sync) {
			super.acquire();
		}
	}

	/**
	 * @see Semaphore#acquire(int)
	 */
	@Override
	public void acquire(int permits) throws InterruptedException {
		synchronized (this.sync) {
			super.acquire(permits);
		}
	}

	/**
	 * @see Semaphore#acquireUninterruptibly()
	 */
	@Override
	public void acquireUninterruptibly() {
		synchronized (this.sync) {
			super.acquireUninterruptibly();
		}
	}

	/**
	 * @see Semaphore#acquireUninterruptibly(int)
	 */
	@Override
	public void acquireUninterruptibly(int permits) {
		synchronized (this.sync) {
			super.acquireUninterruptibly(permits);
		}
	}

	/**
	 * @see Semaphore#tryAcquire()
	 */
	@Override
	public boolean tryAcquire() {
		synchronized (this.sync) {
			return super.tryAcquire();
		}
	}

	/**
	 * @see Semaphore#tryAcquire(int)
	 */
	@Override
	public boolean tryAcquire(int permits) {
		synchronized (this.sync) {
			return super.tryAcquire(permits);
		}
	}

	/**
	 * @see Semaphore#tryAcquire(long, TimeUnit)
	 */
	@Override
	public boolean tryAcquire(long timeout, TimeUnit unit) throws InterruptedException {
		synchronized (this.sync) {
			return super.tryAcquire(timeout, unit);
		}
	}

	/**
	 * @see Semaphore#tryAcquire(int, long, TimeUnit)
	 */
	@Override
	public boolean tryAcquire(int permits, long timeout, TimeUnit unit) throws InterruptedException {
		synchronized (this.sync) {
			return super.tryAcquire(permits, timeout, unit);
		}
	}
	
	/**
	 * This method interrupts all the threads waiting to acquire permits.
	 */
	public void interruptQueuedThreads() {
		synchronized (this.sync) {
			for (Thread thread : this.getQueuedThreads())
				thread.interrupt();
		}
	}

}
